coils (2002-9) unstable; urgency=medium

  * Team Upload.
  * Annotate B-D on perl with :any
  * Add include on /usr/share/dpkg/buildtools.mk
    to make build cross-buildable (Closes: #988553)
  * d/rules: Add $(CPPFLAGS) to compilation flags

 -- Nilesh Patra <nilesh@debian.org>  Sun, 15 Aug 2021 19:25:49 +0530

coils (2002-8) unstable; urgency=medium

  * Standards-Version: 4.5.1 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * No tab in license text (routine-update)
  * Trim trailing whitespace.
  * Set field Upstream-Name in debian/copyright.
  * Remove obsolete field Name from debian/upstream/metadata (already present in
    machine-readable debian/copyright).

 -- Andreas Tille <tille@debian.org>  Sun, 29 Nov 2020 23:13:57 +0100

coils (2002-7) unstable; urgency=medium

  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.2.1
  * Remove trailing whitespace in debian/changelog
  * Remove trailing whitespace in debian/rules

 -- Andreas Tille <tille@debian.org>  Sun, 28 Oct 2018 07:54:31 +0100

coils (2002-6) unstable; urgency=low

  [ Liubov Chuprikova ]
  * Team upload.
  * Fixed typo in override_dh_clean
  * Fix "Please provide autopkgtest" autopkgtest added
    (Closes: #890786)

  [ Andreas Tille ]
  * Standards-Version: 4.1.3
  * debhelper 11

 -- Liubov Chuprikova <chuprikovalv@gmail.com>  Tue, 13 Mar 2018 12:22:29 +0000

coils (2002-5) unstable; urgency=low

  [ Steffen Moeller ]
  * debian/upstream/metadata:
    - Added references to registries
    - yamllint cleanliness

  [ Andreas Tille ]
  * Moved packaging from SVN to Git
  * debhelper 10
  * Standards-Version: 4.1.0 (no changes needed)
  * hardening=+all
  * Fix DEP5 issues
  * Add fake watch file

 -- Andreas Tille <tille@debian.org>  Mon, 18 Sep 2017 09:14:46 +0200

coils (2002-4) unstable; urgency=low

  * debian/upstream:
     - Added DOI + URL
     - Make author field BibTeX compliant
  * debian/control
     - cme fix dpkg-control
     - debhelper 9
     - canonical Vcs fields
  * debian/copyright: DEP5
  * debian/rules: dh
  * removed README.source in favour of adding DEP3 descriptions to patches

 -- Andreas Tille <tille@debian.org>  Mon, 18 Nov 2013 16:47:01 +0100

coils (2002-3) unstable; urgency=low

  * perl script coils-wrap.pl moved from doc to /usr/bin as 'coils-wrap'
  * improved man pages
  * updated upstream URL
  * removed requirement to define COILSDIR env var
  * Laszlo substitutes Steffen as a maintainer.

 -- Laszlo Kajan <lkajan@rostlab.org>  Thu, 27 Oct 2011 12:25:48 +0200

coils (2002-2) unstable; urgency=low

  * debian/control:
    - Team maintained in Debian Med team and added myself to uploaders
    - Removed '[biology]' from short description because we agreed not to use
      this any more
    - Fixed typo in Vcs-Browser field
    - Standards-Version: 3.9.1 (no changes needed)
    - Fixed homepage
  * Added debian/upstream-metadata.yaml

 -- Andreas Tille <tille@debian.org>  Mon, 24 Jan 2011 14:29:17 +0100

coils (2002-1) unstable; urgency=low

  * Initial Release (Closes: #299856).
    updated package from 2005
  * lintian-clean
  * strcpy -> strncpy fixes
  * eliminated some compiler warnings
  * added rudimentary man page

 -- Steffen Moeller <moeller@debian.org>  Sat, 12 Jun 2010 16:03:22 +0200
